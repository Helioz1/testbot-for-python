import discord 
from discord.utils import get
import random
import time
import configparser 
from add_ons.death_game import chance

class Chris_V_Bot(discord.Client):
    #setting
    client = discord.Client()
    game = discord.Activity(type=discord.ActivityType.playing,name ="💯💯💯")

    #Input Discord Bot information into token.ini
    tokenparser = configparser.RawConfigParser()
    tokenparser.read("token.ini")
    token = tokenparser.get("Discord", "Token")


    #status and activity decorator
    @client.event 
    async def on_ready(self): 
        print("We have logged in as {0.user}".format(self))
        await self.change_presence(status=discord.Status.online, activity = self.game)

    #Basic function to talk to Chris_V_Bot
    @client.event
    async def on_message(self, message):
        text = message.channel

        if message.author == self.user:
            return
        
        if message.content == ("Hi"):
            await text.send("Salutations")
        
        if message.content == ("death_game"):
            await text.send(chance())
        
        if message.content == ("death_odds"):
            await text.send("Death: " + str(chance.Death))
            await text.send("Life: " + str(chance.Life))

    #this method kicks the bot in to action from main.py
    def start_Bot(self):
        self.run(self.token)
